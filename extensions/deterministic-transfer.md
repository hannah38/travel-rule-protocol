# Goal

Be able to point to a point in a blockchain to which this transfer pertains.

## Extension header name

In line with the latest spec the name would be: `deterministic-transfer`.

Used in the `api-extensions` header it would look like:

```
api-extensions: deterministic-transfer, some-other-extension
```

## Extension JSON payload

The payload should be placed in the transfer notification POST request, under
the reserved top level `extensions` key. As per latest spec the value should be
a map with a single top level key. This key is `deterministicTransfer`. The
value of the key is dependent on the asset.

The coin specific payload needs to be configured:

- BTC: needs `txId`, `voutIndex` fields.
- ETH: needs `txId` field.
- GRIN: needs `commitmentId` field.
- MONERO: needs `txId`, `address` fields.

## How to handle unknown coins

A client could use this extension and send a value in the 
`deterministicTransfer` key with details of an unknown coin. The server 
must then respond with a `400 - Bad Request` status code. The response body 
should indicate why the request was a bad one.

## Example payloads

### Bitcoin

Types:
- `txId`: string
- `voutIndex`: number

```
POST /assets/BTC/transactions
{
  "ivms101": ...
  "extensions": {
    "deterministicTransfer": {
      "txId": "145856536837127631297866",
      "voutIndex": 1,
    }
  }
}
```

### Grin

https://grinexplorer.net/block/000164236be7525d945e93b61616a94f3a33a7fef8decb51521f6c27a273b2fe

Types:
- `commitmentId`: string
 
```
POST /assets/GRIN/transactions
{
  "ivms101": ...
  "extensions": {
    "deterministicTransfer": {
      "commitmentId": "09b043a394232c443b526e05e1b7bdb14756996e390646e60cdc4f520ff146706f",
    }
  }
}
```

### Ethereum

https://etherscan.io/tx/0x801716c66e015be5b685c0914fd318124371c884d6b56a8af4efd887906e73ee

Types:
- `txId`: string
 
```
POST /assets/ETH/transactions
{
  "ivms101": ...
  "extensions": {
    "deterministicTransfer": {
      "txId": "0x801716c66e015be5b685c0914fd318124371c884d6b56a8af4efd887906e73ee"
    }
  }
}
```

### Monero

https://www.exploremonero.com/transaction/0c975cf31a74c5672d186472c20d2f353f99a36df3f6777a2e7aafcf43baeb03

Types:
- `txId`: string
- `address`: string

```
POST /assets/XMR/transactions
{
  "ivms101": ...
  "extensions": {
    "deterministicTransfer": {
      "txId": "0c975cf31a74c5672d186472c20d2f353f99a36df3f6777a2e7aafcf43baeb03",
      "address": "e5521df93e9bb66e23d5468cdfb447df0b85364034734aa4f0e2c33aa6200902",
    }
  }
}
```

### ZCash

https://blockchair.com/zcash/transaction/ab85f35997fc6d41925b2944a31d2bc595e4652edc6ceb00ee67d6afe22a6bb4

Types:
- `txId`: string
- `voutIndex`: number

```
POST /assets/ZEC/transactions
{
  "ivms101": ...
  "extensions": {
    "deterministicTransfer": {
      "txId": "ab85f35997fc6d41925b2944a31d2bc595e4652edc6ceb00ee67d6afe22a6bb4",
      "voutIndex": 0,
    }
  }
}
```
