## Goal

The goal is to be able to inform the counter-party VASP about the transacted
amount. This information can be used to automatically reject transfers or
to avoid blockchain lookups in case of a trust relationship between two VASPs.

The amount is not strictly necessary for the Travel Rule but it is desirable in
many cases. Most jurisdictions require the amount to be known and not all assets
have this information publicly available.

## Extension header name

In line with v1 on the spec the name would be: `amount`.

Used in the `api-extensions` header it would look like:

```
api-extensions: amount, some-other-extension
```

## Extension JSON payload

The payload should be placed in the transfer notification POST request, under
the reserved top level `extensions` key. As per the v1 spec the value should be
a map with a single top level key. This key is `amount`. The
value of the key is a `number` that denominates the amount in its lowest
denomination (e.g. sat for BTC or wei for ETH).

### Example payload

```
POST /assets/BTC/transactions
{
  "ivms101": ...
  "extensions": {
    "amount": 6839632
  }
}
```

## Interoperability

TRP implementations that don't support the amount extension should ignore that
field.
