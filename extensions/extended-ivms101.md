# Goal

Version 1.0 of the TRP specification defines a message format which is a subset
of the IVMS101 specification. This extension proposes that the transfer
notification endpoint accepts a message containing all of the degrees of freedom
the IVMS101 standard allows.

## Extension header name

In line with v1 of the spec the name would be: `extended-ivms101`.

```
api-extensions: extended-ivms101, some-other-extension
```

## Extension JSON payload

The payload _replaces_ the payload of a regular transfer notification POST
request under the `ivms101` key. The replacement is an extension to include all
of the degrees of freedom as described in the Person component as described in
section 5.2.1 of the IVMS101 spec. The Person component is used in section 6.1 
up until and including 6.4 of the IVMS101 spec.

## Interaction with the Talkative Address Query extension

When this extensions is used in conjunction with the Talkative Address Query
extension the GET request on the address query endpoint should return all
relevant data.

## Example payload

```
POST /assets/BTC/transactions
{
  "IVMS101": {
    "originator": {
      "originatorPersons": [
        {
          "legalPerson": {
            "name": {
              "nameIdentifier": [
                {
                  "primaryIdentifier": "Singapore Airlines",
                  "nameIdentifierType": "LEGL"
                }
              ]
            },
            "geographicAddress": [
              {
                "addressType": "GEOG",
                "streetName": "Potential Street",
                "buildingNumber": "24",
                "buildingName": "Weathering Views",
                "postcode": "91765",
                "townName": "Walnut",
                "countrySubDivision": "California",
                "country": "US"
              }
            ],
            "customerNumber": "1002390"
          }
        }
      ],
      "accountNumber": []
    },
    "beneficiary": {
      "beneficiaryPersons": [
        {
          "naturalPerson": {
            "name": {
              "nameIdentifier": [
                {
                  "primaryIdentifier": "Barnes",
                  "secondaryIdentifier": "Robert",
                  "nameIdentifierType": "LEGL"
                }
              ]
            },
            "nationalIdentification": {
              "nationalIdentifier": "K0000000E",
              "nationalIdentifierType": "CCPT",
              "countryOfIssue": "SG"
            },
            "dateAndPlaceOfBirth": {
              "dateOfBirth": "1900-02-24",
              "placeOfBirth": "Singapore"
            }
          }
        }
      ],
      "accountNumber": [
        "1BVMFfPXJy2TY1x6wm8gow3N5Amw4Etm5h"
      ]
    }
  },
  "extensions": {}
}
```
